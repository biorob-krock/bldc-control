#imports
import numpy as np
import time

#All configurations
odrv0.axis0.requested_state = 1
odrv0.axis0.controller.config.control_mode = CTRL_MODE_CURRENT_CONTROL
odrv0.axis0.controller.current_setpoint = 0
time.sleep(0.5) #in order to not be influenced by previous commands
#set the gains
odrv0.axis0.controller.config.vel_gain = 0.0001
odrv0.axis0.controller.config.vel_integrator_gain = 0.005
odrv0.axis0.controller.config.pos_gain = 10

#define arrays in order to measure all possible values
time_arr = np.array([])
c_set_arr = np.array([])
p_est_arr = np.array([])
v_est_arr = np.array([])
c_est_arr = np.array([])
cpr_count_arr = np.array([])
pos_cpr_arr = np.array([])
acc_arr = np.array([])

#null position calibration
odrv0.axis0.encoder.set_linear_count(0)

#conversion from count/s to rpm
encoder_cpr = 8192 #for CUI Devices AMT112S-V 
counts_to_rpm = 60/encoder_cpr #60 to go from sec to min
motor_kv = 340 #for T-Motor MN5212 KV340

#damped oscillator constants definition
k = 0.12 #spring constant (not diverging w.o. c: 0.015)
c = 0.012 #damping constant (stable w.o. k: 0.01)
In = 0.0001 #0.0007 #virtual inertia (good w. 0.0001)
Kt = 2*np.pi*motor_kv/60 #actually 1/Kt if T=Kt*I
# theta = odrv0.axis0.encoder.pos_estimate in [counts]
# dtheta = odrv0.axis0.encoder.vel_estimate in [counts/s]

#set timer in order to exit loop after X seconds
count = 0
time_end = 30 #in seconds
time_start = time.time()
odrv0.axis0.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL
while True:
	#get position, speed and acceleration values and apply filter onto speed and acceleration
	pos = odrv0.axis0.encoder.pos_estimate
	if (count == 2):
		vel = odrv0.axis0.encoder.vel_estimate
		vel = 0.4*vel + 0.3*old_vel + 0.3*old_old_vel

		acc = (vel-old_vel)/(time.time() - old_time)
		acc = 0.6*acc + 0.4*old_acc
		old_acc = acc

		old_old_vel = old_vel
		old_vel = vel
	elif (count == 1):
		vel = odrv0.axis0.encoder.vel_estimate
		vel = 0.5*(vel + old_vel)

		acc = (vel-old_vel)/(time.time() - old_time)
		old_acc = acc

		count = 2
		old_old_vel = old_vel
		old_vel = vel
	else:
		vel = odrv0.axis0.encoder.vel_estimate
		old_vel = vel

		acc = 0

		count = 1
	old_time = time.time() #used for acc
		
	
	#calculate current setpoint according to differential equation
	odrv0.axis0.controller.current_setpoint = Kt*((-k)*(2*np.pi/encoder_cpr*(pos)) - c*(2*np.pi/encoder_cpr*(vel)) - In*(2*np.pi/encoder_cpr*(acc)))
	
	#acquire data to print at the end
	time_arr = np.append(time_arr,(float(time.time() - time_start)))
	c_set_arr = np.append(c_set_arr,float(odrv0.axis0.controller.current_setpoint))
	p_est_arr = np.append(p_est_arr,float(odrv0.axis0.encoder.pos_estimate))
	v_est_arr = np.append(v_est_arr,counts_to_rpm*float(odrv0.axis0.encoder.vel_estimate))
	c_est_arr = np.append(c_est_arr,float(odrv0.axis0.motor.current_control.Iq_measured))
	cpr_count_arr = np.append(cpr_count_arr,float(odrv0.axis0.encoder.count_in_cpr))
	pos_cpr_arr = np.append(pos_cpr_arr,float(odrv0.axis0.encoder.pos_cpr))
	acc_arr = np.append(acc_arr,2*np.pi/encoder_cpr*float(acc))
	
	#quit the loop after a certain time
	if ((time.time()-time_start)>=time_end):
		break

odrv0.axis0.controller.current_setpoint = 0

#transposing arrays into column vectors and saving in one matrix
time_arr = time_arr.reshape((-1, 1)) #-1 means as many rows as needed
c_set_arr = c_set_arr.reshape((-1, 1))
p_est_arr = p_est_arr.reshape((-1, 1))
v_est_arr = v_est_arr.reshape((-1, 1))
c_est_arr = c_est_arr.reshape((-1, 1))
cpr_count_arr = cpr_count_arr.reshape((-1, 1))
pos_cpr_arr = pos_cpr_arr.reshape((-1, 1))
acc_arr = acc_arr.reshape((-1, 1))

l = np.size(time_arr,0) #to guarantee same length of arrays

print_array = np.concatenate((time_arr,p_est_arr[0:l],v_est_arr[0:l],c_est_arr[0:l],c_set_arr[0:l],cpr_count_arr[0:l],pos_cpr_arr[0:l],acc_arr[0:l]), axis=1)

#save data in .csv file
np.savetxt("HapKit_behavior.csv", print_array , fmt='%f', delimiter=',',header="time,pos_est,vel_est,current_meas,setpoint,count_cpr,pos_cpr, acc")
