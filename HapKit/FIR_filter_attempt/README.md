## Real-time filtering of speed and acceleration through FIR filter

This folder contains a real-time filtering library (from [here](https://github.com/EduardoSebastianRodriguez/REAL-TIME-FIR-FILTER-PYTHON)) that was supposed to be implemented on the HapKit. This seems not to be possible due to the difficulties encountered when trying to use a custom python library on the *odrivetool*. 

### HapKit_filtered.py

The file **HapKit_filtered.py** contains a a version of the Hapkit that should theoretically be able to implement the real-time filter.

### Solution

- Establish a communication to the ODrive through UART or USB and use a development environment such as Eclipse ([guide here](https://docs.odriverobotics.com/configuring-eclipse)) or Visual studio ([guide here](https://docs.odriverobotics.com/configuring-vscode)) to code on the ODrive and load a custom library onto the device. 
- Use a microcontroller that runs the custom library and communicates through UART with the ODrive to send directly commands without needing to modify the ODrive's firmware.
