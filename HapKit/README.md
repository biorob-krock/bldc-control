## Prepare the Hapkit for an experiment

Make sure your motor and your ODrive are correctly connected and set up.
Open **HapKit.py** in your favorite text editor and adapt all the variables and values that you want to test.
Here is a list of all the different values you might want to change:
- the control gains (proportional positional gain, proportional velocity gain, velocity integrator gain)
- encoder cpr value (8192 if you use the AMT-112S-V from CUI Devices)
- motor's Kv value (340 if using the T-Motor)
- the spring coefficient *k*
- the damping coefficient *c*
- the virtual inertia *In*
- time_end: the duration of the simulation in seconds
- the name of the output .csv file - at the end of the script in `np.savetxt()`, for example *hapkit_experiment.csv*

Once all the variables are ready, save HapKit.py

**Safety Measure**: Do not insert the removable element of the handle before having tested the Hapkit without in order to not hurt your fingers!

## Run the HapKit

Open the *odrivetool* in your linux command shell. Navigate to the folder in which *HapKit.py* is deposited through `cd Desktop/bldc-control/HapKit` or something similar.
Check that your ODrive is correctly setup and connected to the *odrivetool*. 
Run `exec(open("HapKit.py").read())` in the odrivetool. This will execute the script.
Now, you can test your HapKit. If you are sure that the settings allow a correct execution of everything, you can play with it.

## Plot the obtained data

Run the script **interpretData_hapkit.py** either in a separate Linux command shell or directly in the *odrivetool* through `exec(open("interpretData_hapkit.py").read())`. The script will ask you to input the name of the file you want to analyze. Input it in the command shell, e.g. *hapkit_experiment.csv*. The plots should open automatically.

## Troubleshooting

Sometimes, the scripts get blocked. This is often the case while plotting through **interpretData_hapkit.py**. To get the program back running, go to the *odrivetool* where the script is running and simply click *Ctrl* on your keyboard. If htat does not hel, you may need to force the end through *Ctrl*+*c* and then restart the *odrivetool* and reconfigure the ODrive.
