# BLDC Motor Control

The idea behind this project is to explore the control of a BLDC motor, the T-Motor, in high torque low speed regime using a low-cost and open-source driver, the ODrive. A future implementation in a robotic system using this setup is aimed.

## Structure of the Repository

To test the setup, a test bench was created by laser cutting and 3D-printing some parts. The mechanical designs can be found in the folder **Mechanical Design**.

Standard step response experiments in position control, velocity control, and current control have been done. To repeat these experiments, go into the **Measurements** folder and follow the instructions. The folder contains Python scripts that execute the step response experiments when they are run inside the *odrivetool* (official software of the ODrive). Also, scripts that interpret the raw experimental data and plot nice graphs can be found in that folder.

A demonstrator in form of a haptic kit has been developed. All the Python scripts can be found in the folder **HapKit** and experiments can be repeated when runningthe scripts inside the *odrivetool*. Also, files that interpret the data gathered during the HapKit experiments can be found in that folder.
