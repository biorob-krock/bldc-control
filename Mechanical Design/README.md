## Mechanical Designe Parts

The mechanical design of all the parts was realized with the free CAD software [FreeCAD](https://www.freecadweb.org/). This folder contains both the FreeCAD files (extension .FCStd) and the corresponding .STEP files that can be opened with any other CAD software.

### Test bench

The test bench consists of two laser cut walls: EncoderWall, on which the encoder is mounted, and MotorWall, on which the Motor is mounted. It also contains the MotorshaftExtension that was 3D-printed using standard FDM. The two walls are separated by spacers that are hold in position through simple screws.

### HapKit Handle

The handle of the HapKit is made of four laser cut parts. The part that is screwed onto the rotor is made of three parts that are glued together: HandleConnectorTop, HandleConnectorTriple, and Handle_OnMotor. The removable extension is called HandleExtension.

![motor image with handle](Motor_Complete.jpg)
