# Troubleshooting the BLDC Setup

### General Troubleshooting

Generally, if something does not seem to work correctly, try to reboot the system using `odrv0.reboot()` and re-calibrate it afterwards using `[axis].requested_state = AXIS_STATE_FULL_CALIBRATION_SEQUENCE`, where [axis] stands for odrv0.axis0 or odrv0.axis1 depending on whether you are using the M0 or the M1 output to drive the motor.

If the motor seems to not work after re-booting and re-calibrating it, then use the `dump_errors(odrv0)` command to check what has happened. Often, the errors resulting from bad configuration can be found like that. To decompose the error code, refer to the ODrive website [here](https://docs.odriverobotics.com/troubleshooting). To clear the errors you can use `dump_errors(odrv0, True)`.

### What to do if the setup exhibits unexpected behavior

When experiencing unexpected behavior that leads to a shutdown of the system, this is most probably do to an overvoltage on the bus line. The system won't start and you need to re-boot and re-calibrate it to use it again. There are two different approaches to find out what happened:
- Use `dump_errors(odrv0)` command and refer to the error references [here](https://docs.odriverobotics.com/troubleshooting).
- Re-boot, re-calibrate, and try to duplicate the error while running the liveplotter tool and verifying what the suspected measured values are doing. Example if monitoring the bus voltage, and the motor current: `start_liveplotter(lambda:[odrv0.vbus_voltage, [axis].motor.current_control.Iq_measured])`.

Generally, an unexpected behavior can be resolved by controlling the limits on the used axis. For example, when you are implementing a spring like beahvior in the HapKit, too high an external input will cause the system to diverge if the current limit is set too low as the controller will not be able to counter the reaction and this will make the system diverge until the security shutdown of the ODrive's firmware intervenes. To solve these issues, check:
- `[axis].motor.config.current_lim`
- `[axis].controller.config.vel_limit`

It could also be observed, when applying too high a step in velocity control that the bus voltage overshoots and the system shuts down. To avoid this overshoot that is due to the high step required, please use the reamped velocity control mode. Thus you can impose a ramp to follow to reach a high velocity. This has proven to work well. The approach can be found [here, nearly at the bottom of the page](https://docs.odriverobotics.com/).

### What to do if re-booting and re-calibrating fails
Re-booting the system sometimes outputs a ChannelBrokenException. This did not seem to cause any further problem and can in most cases be ignored on M0. It does not happen on M1 (more later). Booting and calibrating the setup cause the emission of an acoustic signal by the T-Motor. If there is no such signal, then something is wrong. This could be due to two reasons:
- The system is too hot. Overheating occurred and some internal security measure prevents the system from restarting. Check the T-Motor if it is hot (I never burned my fingers, but it is clearly noticeable). If so, wait until it cooled down and try again. This can take 10 minutes.
- There is a short-circuit somewhere in the system. Check all your connections and try again. This can be visualized by a voltage drop on the power supply.
- The short-circuit lies in the ODrive. Change the output port from M0 to M1 or vice-versa. The broken port can probably not be used anymore.

### Biorob ready system used for the semester project
The ODrive's channel M0 is broken. Do not use it. Use M1 instead. If needed, please change all odrv0.axis0 in the codes from this GitLab to odrv0.axis1 to guarantee correct execution.