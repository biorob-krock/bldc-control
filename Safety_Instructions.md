# Safety Instructions

Using the current setup (end of  fall 2019) is a good idea if one tries to not exerce too much power. Else, as the setup is not fixed to a table or other supporting structure, it is not recommended to go to try to reach the motor's limits as it seems not very safe. Each user needs to estimate by himself or herself if the required actuations do or do not require a mechanical adaptation of the setup.
**Pay attention to what you try to implement and adapt the mechanics to it**.

## In case of something going wrong (high insatbilities)
- `Ctrl + C` in order to force-close a script in the odrivetool.
- `[axis].requested_state = 1` to exit whatever control mode (does not control the motor anymore, and the motor just stops abruptly). This is only possible if the odrivetool is not executing a script, else you would need to stop it first as explained in the previous point.
- Unplug the power supply. The most abrupt way that may cause sparks. Not safest option, so only use if nothing else can be done.

## Tuning the gains or HapKit coefficients
- Do not use the extended handle. If something goes wrong, it will crash against a spacer.
- Do not put your fingers between spacers and rotor due to the rotating part of the handle attached to the rotor.
- The motor might go unstable, be ready to change state by `[axis].requested_state = 1`.

## Things not to do
- `[axis].motor.config.direction = ###`, where ### stands for +1 or -1. Increasing this value (>1 or <-1) leads to high instabilities.
- Do not use too high gains for the setting. Start at low gains ( to give an order of magnitude take e.g. Kpp = 10, Kvp = 0.00005, KVi = 0.00005) and increase them stepwise until you reach the limits.
- The same goes for the HapKit parameters *k*, *c*, and *I*. Instead, choose small values (e.g. *k* = 0.01[kgm2s-2],*c* = 0.001[kgm2s-1], *I* = 0.0001[kgm2]) before increasing them stepwise to avoid really high and unpredictable instabilities.
- Do not expect the system to react well to big step responses. The setup risks to move in an unsafe way due to high accelerations involved. Demanding too high a step can create an overvoltage on the ODrive due to the back-emf that causes a security shutdown. This is especially true if you are going from a high velocity (near nominal value) back to zero. Use a velocity ramp instead.