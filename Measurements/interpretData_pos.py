#imports
import numpy as np
import matplotlib.pyplot as plt

#get filename through user
path = input("Input filepath of the .csv file to read: ")
print(path)

#open a csv file without header
data = np.loadtxt(path, delimiter=',',skiprows = 1)

#get all data into arrays
t = data[:,0]
p_est = data[:,1]
v_est = data[:,2]
c_meas = data[:,3]
set_val = data[:,4]
cpr_count = data[:,5]
pos_cpr = data[:,6]

#plot all the data

fig, axs = plt.subplots(2, 2)
fig.suptitle(path)

axs[0, 0].plot(t,p_est,'r',t,set_val,'b')
axs[0, 0].set(xlabel='time [s]')
axs[0, 0].set(ylabel='pos [counts]')
axs[0, 0].set_title("Position")
axs[0, 0].legend(('measured value','setpoint'), loc = 'upper left')

axs[0, 1].plot(t,v_est)
axs[0, 1].set(xlabel='time [s]')
axs[0, 1].set(ylabel='velocity [rpm]')
axs[0, 1].set_title("Speed")

axs[1, 0].plot(t,c_meas)
axs[1, 0].set(xlabel='time [s]')
axs[1, 0].set(ylabel='current [A]')
axs[1, 0].set_title("Current")

axs[1, 1].plot(t,pos_cpr)
axs[1, 1].set(xlabel='time [s]')
axs[1, 1].set(ylabel='pos in cpr')
axs[1, 1].set_title("Encoder cpr values")

plt.show()
