#Imports
import time
import numpy as np

#All configurations
odrv0.axis0.requested_state = 1
odrv0.axis0.controller.config.control_mode = CTRL_MODE_CURRENT_CONTROL
odrv0.axis0.controller.current_setpoint = 0
time.sleep(0.5)

####  set the gains ####
odrv0.axis0.controller.config.vel_gain = 0.0001
odrv0.axis0.controller.config.vel_integrator_gain = 0.005
odrv0.axis0.controller.config.pos_gain = 10	

#conversion from count/s to rpm
encoder_cpr = 8192 #for CUI Devices AMT112S-V 
counts_to_rpm = 60/encoder_cpr #60 to go from sec to min

#define arrays: one for time (x-axis) and one or more for the measured values
time_arr = np.array([])
c_set_arr = np.array([])

p_est_arr = np.array([])
v_est_arr = np.array([])
c_est_arr = np.array([])
cpr_count_arr = np.array([])
pos_cpr_arr = np.array([])

#taking the measurement
data_rate = 10 # 10 measurements per second - inverse of timestep
timestep = 1/data_rate
duration_0 = 5 # taking measures during 5s
duration_1 = 10 # taking measures during 10s
odrv0.axis0.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL
start_time = time.time()
while True:
	time_arr = np.append(time_arr,(float(time.time() - start_time)))
	c_set_arr = np.append(c_set_arr,float(odrv0.axis0.controller.current_setpoint))

	p_est_arr = np.append(p_est_arr,float(odrv0.axis0.encoder.pos_estimate))
	v_est_arr = np.append(v_est_arr,counts_to_rpm*float(odrv0.axis0.encoder.vel_estimate))
	c_est_arr = np.append(c_est_arr,float(odrv0.axis0.motor.current_control.Iq_measured))
	cpr_count_arr = np.append(cpr_count_arr,float(odrv0.axis0.encoder.count_in_cpr))
	pos_cpr_arr = np.append(pos_cpr_arr,float(odrv0.axis0.encoder.pos_cpr))

	#time.sleep(timestep) #blocking code: comment out if necessary
	if ((time.time()-start_time)>=duration_0):
		break

odrv0.axis0.controller.current_setpoint = 5

while True:
	time_arr = np.append(time_arr,(float(time.time() - start_time)))
	c_set_arr = np.append(c_set_arr,float(odrv0.axis0.controller.current_setpoint))

	p_est_arr = np.append(p_est_arr,float(odrv0.axis0.encoder.pos_estimate))
	v_est_arr = np.append(v_est_arr,counts_to_rpm*float(odrv0.axis0.encoder.vel_estimate))
	c_est_arr = np.append(c_est_arr,float(odrv0.axis0.motor.current_control.Iq_measured))
	cpr_count_arr = np.append(cpr_count_arr,float(odrv0.axis0.encoder.count_in_cpr))
	pos_cpr_arr = np.append(pos_cpr_arr,float(odrv0.axis0.encoder.pos_cpr))

	#time.sleep(timestep) #blocking code: comment out if necessary
	if ((time.time()-start_time)>=duration_1):
		break

time.sleep(0.5)
odrv0.axis0.controller.current_setpoint = 0

#transposing arrays into column vectors and saving in one matrix
time_arr = time_arr.reshape((-1, 1)) #-1 means as many rows as needed
c_set_arr = c_set_arr.reshape((-1, 1))
p_est_arr = p_est_arr.reshape((-1, 1))
v_est_arr = v_est_arr.reshape((-1, 1))
c_est_arr = c_est_arr.reshape((-1, 1))
cpr_count_arr = cpr_count_arr.reshape((-1, 1))
pos_cpr_arr = pos_cpr_arr.reshape((-1, 1))

l = np.size(time_arr,0) #to guarantee same length of arrays

print_array = np.concatenate((time_arr,p_est_arr[0:l],v_est_arr[0:l],c_est_arr[0:l],c_set_arr[0:l],cpr_count_arr[0:l],pos_cpr_arr[0:l]), axis=1)

#saving values in file
np.savetxt("currentCtrlStep5.csv", print_array , fmt='%f', delimiter=',',header="time,pos_est,vel_est,current_meas,setpoint,count_cpr,pos_cpr")
