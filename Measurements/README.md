# Measurements - Step Responses

This folder contains all the different step response experiments scripts for position, velocity, and current control that are ready to be repeated by the user.

## Before the Experiments

- setup the correct pole pairs, brake resistor values, etc.
- configure the T-Motor using `odrv0.axis0.requested_state = AXIS_STATE_FULL_CALIBRATION_SEQUENCE` in the *odrivetool*
- set current limit and velocity limit to a high enough value to run the experiment. Use `odrv0.axis0.motor.config.current_lim = #high enough value`and `odrv0.axis0.controller.config.vel_limit = #high enough value` in the *odrivetool*

## Run the Experiments

In each of the folders Pos, Vel, and Current, there will be a file **takeMeasures_**current/vel/pos**CtrlStep.py** which can be executed in the *odrivetool*. To do so, follow these steps:
- open *odrivetool* and check that the ODrive is connected
- navigate to the correct folder through `cd bldc-control/Measurements/Vel`or similar
- open the **takeMeasures.py** script and adaptit to your needs (e.g. change gains, times, etc.)
- change the name of the output .csv-file at the end of the script if you don not want it to overwrite the previously present file
- execute the following command in the linux command line `exec(open(path/to/Python/Script/.py).read())` with the correct python script
- wait until the experiment finishes

## Plot the data

Use the **interpretData.py** scripts of this folder. There is one for position control experiments, one for velocity control experiments, and one for current control experiments. These files will ask you to input the path to the .csv-file to analyze. Do this in the command line and the scripts will output all the measurements taken during the experiments.


