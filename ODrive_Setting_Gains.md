# Setting ODrive gains

### ODrive Control Architecture

The control structure implemented in the ODrive looks like this:
![ODrive Control Architecture](ODrive_ControlArchitecture.PNG)

There are three stages: position control, velocity control, and current control.
The positional loop is a simple P loop using the following mathematics:
- pos_error = pos_setpoint - pos_feedback
- vel_cmd = pos_error * pos_gain + vel_feedforward

The velocity loop is a PI loop with:
- vel_error = vel_cmd - vel_feedback
- current_integral += vel_error * vel_integrator_gain
- current_cmd = vel_error * vel_gain + current_integral + current_feedforward

The control loop is a PI loop as well:
- current_error = current_cmd - current_fb
- voltage_integral += current_error * current_integrator_gain
- voltage_cmd = current_error * current_gain + voltage_integral (+ voltage_feedforward when we have motor model)

### Gain setting

The ODrive has not yet implemented any completely automatic gain tuning. Hence, the proportional positional gain, the proportional speed gain, as well as the integrator speed gain need to be tuned manually by the user. The current gains are not tuned by the user. This is done automatically. According to the ODrive developers, the automatic gain tuning will be implemented in future. Until then, it has to be performed manually.

First, you need to tune the velocity gains. Then, you will tune the positional gain. The procedure to set the gains is the following:
- Set velocity integrator gain Kvi and positional gain Kpp to 10 or twenty (should be stable, else change the value to something lower)
- Make sure to have a stable system with only Kvp different than zero (it will probably follow a velocity step response with an offset)
- Increase Kvp stepwise until you get instability.
- Back down until you have a stable system again. Make sure to allow a security margin in the case of an unsuccessful calibration or other unpredictable issues.
- Set your integrator gain Kvi at 0.5 * bandwidth * Kvp. If you do not know your bandwidth, increse Kvi until you get a good behavior without overshoot.
- Increase positional gain Kpp until you see some overshoot
- Back down Kpp until there is no overshoot anymore

### Stable values found for the configuration at the end of the semester project using a T-Motor MN5212 KV340 BLDC motor and the CUI Devices AMT112S-V incremental encoder
- Kpp = 10
- Kvp = 0.0001
- Kvi = 0.005

##### For the HapKit
- k = 0.12 (only if with c = 0.12, else decrease k by a factor 2 at least)
- c = 0.012
- I = 0.0001 (if k = 0 and c = 0, then I can be increased to 0.0005 or a bit more, depends on the noise)
